import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider { 

  //apiUrl = "https://swapi.co/api/starships";
  //apiUrl = "https://desafio-mobility.herokuapp.com/products.json";
  apiUrl = "assets/data/products.json";

  constructor(public http: HttpClient) {  
    //console.log('Hello RestProvider Provider');
  }

  getData() {
    return new Promise(resolve => {this.http.get(this.apiUrl).subscribe(data => resolve(data))});
  }


}
