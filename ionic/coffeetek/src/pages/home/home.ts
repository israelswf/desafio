import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

import { DetailsPage } from '../details/details';
 
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage { 
  data:any;
  item:any;
  products: any;
  constructor(public navCtrl: NavController, public restProvider: RestProvider) {
    this.getData();
  }

  openDetails(item) {
    this.navCtrl.push(DetailsPage, {item}); 
  }

  getData() {
    this.restProvider.getData()
    .then(data => {
      this.data = data;
      console.log(this.data);
      this.products = ((this.data.products));
    });
    }
 
}
